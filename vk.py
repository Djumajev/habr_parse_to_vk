# -*- coding: utf-8 -*-
import vk_api
import html2text
import requests
from BeautifulSoup import BeautifulSoup
from bs4 import BeautifulSoup
import json
from time import sleep

def auth_handler():
    """ При двухфакторной аутентификации вызывается эта функция.
    """
    key = input("Enter authentication code: ")
    remember_device = True
    return key, remember_device

def auth():
    login = ''
    password = ''
    token = ''
    vk_session = vk_api.VkApi(password=password, login=login, token=token, auth_handler=auth_handler)
    vk_session.auth()

    #Залогинились на
    vk = vk_session.get_api()
    return vk

def post(txt, link):
    parsed_html = BeautifulSoup(txt)
    post = str(parsed_html.body.find('div', attrs={'class':'post__text'}))

    h = html2text.HTML2Text()
    post = h.handle(post.decode('utf8'))
    
    post = link + '\n' + post 

    owner = '-179882264'
    auth().wall.post(owner_id=owner, message=post)

def getLinkReady(link):
    response = requests.get(link)

    if response.status_code == 200:
       post(response.content, link)
       return True
    return False

def getNewPosts():
    #Get new posts
    linkNewPosts = 'https://habr.com/ru/all/'
    allPosts = []

    response = requests.get(linkNewPosts)

    if response.status_code == 200:
       parsed_html = BeautifulSoup(response.content)
       for a in parsed_html.find_all('a', { "class":'post__title_link'}):
           allPosts.append(a['href'])
    else:
        print "Cant get new posts, check your connection!"
        exit

    #Already posted posts
    with open('last.json') as f:
        posted = json.load(f)

    #if succes -> write to json that OK
    for p in allPosts:
        if p in posted:
            print p + '/ -> is already posted' 
        else:
            if getLinkReady(p):
                print p + '/ -> posted' 
                posted.append(p)
                with open('last.json', 'w') as outfile:
                    json.dump(posted, outfile)
            else:
                print p + '/ -> canot be loaded :(' 

getNewPosts()
